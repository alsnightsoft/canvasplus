package com.alsnightsoft.vaadin.widgets.canvasplus;

import java.util.ArrayList;
import java.util.List;

import com.alsnightsoft.vaadin.widgets.canvasplus.client.canvasplus.CanvasPlusClientRpc;
import com.alsnightsoft.vaadin.widgets.canvasplus.client.canvasplus.CanvasPlusServerRpc;
import com.vaadin.shared.MouseEventDetails;
import com.vaadin.ui.AbstractComponent;

public class CanvasPlus extends AbstractComponent {

	private static final long serialVersionUID = 1793420664283087039L;

	private final List<CanvasClickListener> clickListeners = new ArrayList<CanvasClickListener>();
	private final List<CanvasClickUpListener> clickUpListeners = new ArrayList<CanvasClickUpListener>();
	private final List<CanvasClickDownListener> clickDownListeners = new ArrayList<CanvasClickDownListener>();
	private final List<CanvasMouseMoveListener> moveListeners = new ArrayList<CanvasMouseMoveListener>();
	private final List<CanvasImageLoadListener> imgLoadListeners = new ArrayList<CanvasImageLoadListener>();

	private final CanvasPlusClientRpc rpc = getRpcProxy(CanvasPlusClientRpc.class);

	public CanvasPlus() {
		registerRpc(new CanvasPlusServerRpc() {

			private static final long serialVersionUID = 3531861726699580635L;

			@Override
			public void onClick(MouseEventDetails mouseDetails) {
				fireClick(mouseDetails);
			}

			@Override
			public void onClickDown(MouseEventDetails mouseDetails) {
				fireClickDown(mouseDetails);
			}

			@Override
			public void onClickUp(MouseEventDetails mouseDetails) {
				fireClickUp(mouseDetails);
			}

			@Override
			public void onMouseMove(MouseEventDetails mouseDetails) {
				fireMouseMove(mouseDetails);
			}

			@Override
			public void imgLoaded() {
				fireImgLoaded();
			}
		});
	}

	private void fireClick(MouseEventDetails mouseDetails) {
		for (CanvasClickListener listener : clickListeners) {
			listener.onClick(mouseDetails);
		}
	}

	private void fireClickUp(MouseEventDetails mouseDetails) {
		for (CanvasClickUpListener listener : clickUpListeners) {
			listener.onClickUp(mouseDetails);
		}
	}

	private void fireClickDown(MouseEventDetails mouseDetails) {
		for (CanvasClickDownListener listener : clickDownListeners) {
			listener.onClickDown(mouseDetails);
		}
	}

	private void fireMouseMove(MouseEventDetails mouseDetails) {
		for (CanvasMouseMoveListener listener : moveListeners) {
			listener.onMove(mouseDetails);
		}
	}

	private void fireImgLoaded() {
		for (CanvasImageLoadListener listener : imgLoadListeners) {
			listener.onImageLoad();
		}
	}

	/**
	 * Interface to implement a click listener.
	 */
	public interface CanvasClickListener {
		public void onClick(MouseEventDetails mouseDetails);
	}

	/**
	 * Interface to implement a click down listener.
	 */
	public interface CanvasClickDownListener {
		public void onClickDown(MouseEventDetails mouseDetails);
	}

	/**
	 * Interface to implement a click up listener.
	 */
	public interface CanvasClickUpListener {
		public void onClickUp(MouseEventDetails mouseDetails);
	}

	/**
	 * Interface to implement a image load listener.
	 */
	public interface CanvasImageLoadListener {
		public void onImageLoad();
	}

	/**
	 * Interface to implement a mouse move listener.
	 */
	public interface CanvasMouseMoveListener {
		public void onMove(MouseEventDetails mouseDetails);
	}

	/**
	 * Adds the Canvas click listener.
	 * 
	 * @param listener
	 *            the Listener to be added.
	 */
	public void addClickListener(CanvasClickListener listener) {
		clickListeners.add(listener);
	}

	/**
	 * Removes the Canvas click listener.
	 * 
	 * @param listener
	 *            the Listener to be removed.
	 */
	public void removeClickListener(CanvasClickListener listener) {
		if (clickListeners.contains(listener)) {
			clickListeners.remove(listener);
		}
	}

	/**
	 * Adds the Canvas click up listener.
	 * 
	 * @param listener
	 *            the Listener to be added.
	 */
	public void addClickUpListener(CanvasClickUpListener listener) {
		clickUpListeners.add(listener);
	}

	/**
	 * Removes the Canvas click up listener.
	 * 
	 * @param listener
	 *            the Listener to be removed.
	 */
	public void removeClickUpListener(CanvasClickUpListener listener) {
		if (clickUpListeners.contains(listener)) {
			clickUpListeners.remove(listener);
		}
	}

	/**
	 * Adds the Canvas click down listener.
	 * 
	 * @param listener
	 *            the Listener to be added.
	 */
	public void addClickDownListener(CanvasClickDownListener listener) {
		clickDownListeners.add(listener);
	}

	/**
	 * Removes the Canvas click down listener.
	 * 
	 * @param listener
	 *            the Listener to be removed.
	 */
	public void removeClickDownListener(CanvasClickDownListener listener) {
		if (clickDownListeners.contains(listener)) {
			clickDownListeners.remove(listener);
		}
	}

	/**
	 * Adds the Canvas mouse move listener.
	 * 
	 * @param listener
	 *            the Listener to be added.
	 */
	public void addMouseMoveListener(CanvasMouseMoveListener listener) {
		moveListeners.add(listener);
	}

	/**
	 * Removes the Canvas mouse move listener.
	 * 
	 * @param listener
	 *            the Listener to be removed.
	 */
	public void removeMouseMoveListener(CanvasMouseMoveListener listener) {
		if (moveListeners.contains(listener)) {
			moveListeners.remove(listener);
		}
	}

	/**
	 * Adds the Canvas image load listener.
	 * 
	 * @param listener
	 *            the Listener to be added.
	 */
	public void addImageLoadListener(CanvasImageLoadListener listener) {
		imgLoadListeners.add(listener);
	}

	/**
	 * Removes the Canvas image load listener.
	 * 
	 * @param listener
	 *            the Listener to be removed.
	 */
	public void removeImageLoadListener(CanvasImageLoadListener listener) {
		if (imgLoadListeners.contains(listener)) {
			imgLoadListeners.remove(listener);
		}
	}

	/**
	 * Load and cache images before call draw.
	 * 
	 * @see drawImage
	 */
	public void loadImages(String[] urls) {
		rpc.loadImages(urls);
	}

	/**
	 * Draw an image, other canvas, or video onto this canvas.
	 * 
	 * @param url
	 *            URL of the image.
	 * @param offsetX
	 *            the X coordinate for upper left corner of drawing area.
	 * @param offsetY
	 *            the Y coordinate for upper left corner of drawing area.
	 */
	public void drawImage(String url, Double offsetX, Double offsetY) {
		rpc.drawImage(url, offsetX, offsetY);
	}

	/**
	 * Draw an image, other canvas, or video onto this canvas.
	 * 
	 * @param url
	 *            URL of the image
	 * @param offsetX
	 *            the X coordinate for upper left corner of drawing area.
	 * @param offsetY
	 *            the Y coordinate for upper left corner of drawing area.
	 * @param width
	 *            the width of the drawing area.
	 * @param height
	 *            the height of the drawing area.
	 */
	public void drawImage(String url, Double offsetX, Double offsetY,
			Double imageWidth, Double imageHeight) {
		rpc.drawImage2(url, offsetX, offsetY, imageWidth, imageHeight);
	}

	/**
	 * Draw a portion of an image, other canvas, or video onto this canvas.
	 * 
	 * @param url
	 *            URL of the image.
	 * @param sourceX
	 *            the X coordinate of the upper left corner of the area in the
	 *            source image to draw.
	 * @param sourceY
	 *            the Y coordinate of the upper left corner of the area in the
	 *            source image to draw.
	 * @param sourceWidth
	 *            the width of the area in the source image to draw.
	 * @param sourceHeight
	 *            the height of the area in the source image to draw.
	 * @param destX
	 *            the X coordinate for upper left corner of drawing area.
	 * @param destY
	 *            the Y coordinate for upper left corner of drawing area.
	 * @param destWidth
	 *            the width of the drawing area.
	 * @param destHeight
	 *            the height of the drawing area.
	 */
	public void drawImage(String url, Double sourceX, Double sourceY,
			Double sourceWidth, Double sourceHeight, Double destX,
			Double destY, Double destWidth, Double destHeight) {
		rpc.drawImage3(url, sourceX, sourceY, sourceWidth, sourceHeight, destX,
				destY, destWidth, destHeight);
	}

	/**
	 * Fills the current drawing (path). The default color is black. Use the
	 * fillStyle property to fill with another color/gradient.
	 */
	public void fill() {
		rpc.fill();
	}

	/**
	 * Draw a "filled" rectangle. The default color of the fill is black.
	 * 
	 * Use the fillStyle property to set a color, gradient, or pattern used to
	 * fill the drawing.
	 * 
	 * @param startX
	 *            the X coordinate for upper left corner of fill area.
	 * @param startY
	 *            the Y coordinate for upper left corner of fill area.
	 * @param width
	 *            the width of the fill area.
	 * @param height
	 *            the height of the fill area.
	 */
	public void fillRect(Double startX, Double startY, Double rectWidth,
			Double rectHeight) {
		rpc.fillRect(startX, startY, rectWidth, rectHeight);
	}

	/**
	 * Draw filled text on the canvas. The default color of the text is black.
	 * 
	 * Use the font property to specify font and font size, and use the
	 * fillStyle property to render the text in another color/gradient.
	 * 
	 * @param text
	 *            the text to drawn.
	 * @param x
	 *            the X coordinate of the upper left corner of the drawing area.
	 * @param y
	 *            the Y coordinate of the upper left corner of the drawing area.
	 * @param maxWidth
	 *            the maximum allowed width of the text, in pixels.
	 */
	public void fillText(String text, Double x, Double y, Double maxWidth) {
		rpc.fillText(text, x, y, maxWidth);
	}

	/**
	 * Set the current font properties for text content on the canvas. Uses the
	 * same syntax as the CSS font property.
	 * 
	 * @param font
	 *            new font
	 */
	public void setFont(String font) {
		rpc.setFont(font);
	}

	/**
	 * Sets the text baseline used when drawing text. Possible values are:
	 * "top", "hanging", "middle", "alphabetic", "ideographic" or "bottom".
	 * Default value is "alphabetic".
	 * 
	 * The {@link #fillText(String, double, double, double)} will use the
	 * specified textBaseline value when positioning the text on the canvas.
	 * 
	 * @param textBaseline
	 *            the new text baseline.
	 */
	public void setTextBaseline(String textBaseline) {
		rpc.setTextBaseline(textBaseline);
	}

	/**
	 * Adds a new point and creates a line from that point to the last specified
	 * point in the canvas.
	 * 
	 * Use the {@link #stroke()} method to actually draw the path on the canvas.
	 * 
	 * @param x
	 *            the x coordinate where to create a line to.
	 * @param y
	 *            the y coordinate where to create a line to.
	 */
	public void lineTo(double x, double y) {
		rpc.lineTo(x, y);
	}

	/**
	 * Move the path to the specified point in the canvas, without creating a
	 * line.
	 * 
	 * Use the {@link #stroke()} method to actually draw the path on the canvas.
	 * 
	 * @param x
	 *            the x coordinate where to move the path to.
	 * @param y
	 *            the y coordinate where to move the path to.
	 */
	public void moveTo(double x, double y) {
		rpc.moveTo(x, y);
	}

	/**
	 * Adds a point to the current path by using the specified control points
	 * that represent a quadratic BŽzier curve.
	 * 
	 * A quadratic BŽzier curve requires two points. The first point is a
	 * control point that is used in the quadratic BŽzier calculation and the
	 * second point is the ending point for the curve. The starting point for
	 * the curve is the last point in the current path.
	 * 
	 * @param cpx
	 *            The X coordinate of the BŽzier control point.
	 * @param cpy
	 *            The Y coordinate of the BŽzier control point.
	 * @param x
	 *            The X coordinate of the ending point.
	 * @param y
	 *            The Y coordinate of the ending point.
	 */
	public void quadraticCurveTo(Double cpx, Double cpy, Double x, Double y) {
		rpc.quadraticCurveTo(cpx, cpy, x, y);
	}

	/**
	 * Creates a rectangle to the current path.
	 * 
	 * Use the {@link #stroke()} method to actually draw the path on the canvas.
	 * 
	 * @param startX
	 *            The X coordinate of the upper-left corner of the rectangle.
	 * @param startY
	 *            The Y coordinate of the upper-left corner of the rectangle.
	 * @param width
	 *            the width of the rectangle.
	 * @param height
	 *            the height of the rectangle {@link #fill()} methods to
	 *            actually draw the rectangle on the canvas.
	 */
	public void rect(Double startX, Double startY, Double rectWidth,
			Double rectHeight) {
		rpc.rect(startX, startY, rectWidth, rectHeight);
	}

	/**
	 * Rotates the current drawing. The rotation will only affect drawings made
	 * AFTER the rotation is done.
	 * 
	 * @param angle
	 *            the angle to rotate by, in radians.
	 */
	public void rotate(Double angle) {
		rpc.rotate(angle);
	}

	/**
	 * Sets the color, gradient, or pattern used to fill the drawing.
	 * 
	 * @param color
	 *            the new fill style.
	 */
	public void setFillStyle(String color) {
		rpc.setFillStyle(color);
	}

	/**
	 * Sets the style of the end caps for a line. Possible values are "butt",
	 * "round" and "square".
	 * 
	 * "round" and "square" make the lines slightly longer.
	 * 
	 * @param lineCap
	 *            the new line cap.
	 */
	public void setLineCap(String lineCap) {
		rpc.setLineCap(lineCap);
	}

	/**
	 * Sets the type of corner created, when two lines meet. Possible values are
	 * "bevel", "round" and "miter".
	 * 
	 * The "miter" value is affected by the miterLimit property, which you can
	 * set using the {@link #setMiterLimit(double)} method.
	 * 
	 * @param lineJoin
	 *            the new line join.
	 */
	public void setLineJoin(String lineJoin) {
		rpc.setLineJoin(lineJoin);
	}

	/**
	 * Sets the current line width, in pixels.
	 * 
	 * @param width
	 *            the new line width.
	 */
	public void setLineWidth(Double width) {
		rpc.setLineWidth(width);
	}

	/**
	 * Sets the maximum miter length. The miter length is the distance between
	 * the inner corner and the outer corner where two lines meet. The
	 * miterLimit property works only if the lineJoin attribute is "miter".
	 * 
	 * The miter length grows bigger as the angle of the corner gets smaller. To
	 * prevent the miter length from being too long, we can use the miterLimit
	 * property.
	 * 
	 * If the miter length exceeds the miterLimit value, the corner will be
	 * displayed as lineJoin type "bevel"
	 * 
	 * @param miterLimit
	 *            the new miter limit.
	 */
	public void setMiterLimit(Double miterLimit) {
		rpc.setMiterLimit(miterLimit);
	}

	/**
	 * Draw the path you have defined with eg {@link #moveTo(double, double)}
	 * and {@link #lineTo(double, double)} methods.
	 * 
	 * The default color is black. Use the {@link #setStrokeStyle(String)} to
	 * draw with another color/gradient.
	 */
	public void stroke() {
		rpc.stroke();
	}

	/**
	 * Draw a rectangle (no fill). The default color of the stroke is black.
	 * 
	 * Use the {@link #setStrokeStyle(String)} method to set a color, gradient,
	 * or pattern to style the stroke.
	 * 
	 * @param startX
	 *            the X coordinate of the upper left corner of the rectangle.
	 * @param startY
	 *            the X coordinate of the upper left corner of the rectangle.
	 * @param width
	 *            the width of the rectangle.
	 * @param height
	 *            the height of the rectangle.
	 */
	public void strokeRect(Double startX, Double startY, Double strokeWidth,
			Double strokeHeight) {
		rpc.strokeRect(startX, startY, strokeWidth, strokeHeight);
	}

	/**
	 * Each object on the canvas has a current transformation matrix.
	 * 
	 * This method replaces the current transformation matrix. It multiplies the
	 * current transformation matrix with the matrix described by:
	 * 
	 * a c e
	 * 
	 * b d f
	 * 
	 * 0 0 1
	 * 
	 * In other words, the it lets you scale, rotate, move, and skew the current
	 * context.
	 * 
	 * The transformation will only affect drawings made after the method is
	 * called.
	 * 
	 * @param m11
	 *            Scales the drawing horizontally.
	 * @param m12
	 *            Skew the drawing horizontally.
	 * @param m21
	 *            Skew the drawing vertically.
	 * @param m22
	 *            Scales the drawing vertically.
	 * @param dx
	 *            Moves the the drawing horizontally.
	 * @param dy
	 *            Moves the the drawing vertically {@link #rotate(double)},
	 *            {@link #scale(double, double)} or
	 *            {@link #translate(double, double)}.
	 * 
	 *            For example: If you already have set your drawing to scale by
	 *            two, and the
	 *            {@link #transform(double, double, double, double, double, double)}
	 *            method scales your drawings by two, your drawings will now
	 *            scale by four.
	 */
	public void transform(Double m11, Double m12, Double m21, Double m22,
			Double dx, Double dy) {
		rpc.transform(m11, m12, m21, m22, dx, dy);
	}

	/**
	 * This method creates an arc/curve (used to create circles, or parts of
	 * circles). To create a circle with arc(): Set start angle to 0 and end
	 * angle to 2 * Math.PI.
	 * 
	 * Use the {@link #stroke()} or {@link #fill()} method to actually draw the
	 * arc on the canvas.
	 * 
	 * @param x
	 *            The x-coordinate of the center of the circle.
	 * @param y
	 *            The y-coordinate of the center of the circle.
	 * @param radius
	 *            the radius of the circle, in radians.
	 * @param startAngle
	 *            The starting angle, in radians (0 is at the 3 o'clock position
	 *            of the arc's circle).
	 * @param endAngle
	 *            The end angle, in radians.
	 * @param antiClockwise
	 *            Specifies whether the drawing should be counterclockwise or
	 *            clockwise.
	 */
	public void arc(Double x, Double y, Double radius, Double startAngle,
			Double endAngle, Boolean antiClockwise) {
		rpc.arc(x, y, radius, startAngle, endAngle, antiClockwise);
	}

	/**
	 * This method remaps the (0,0) position on the canvas.
	 * 
	 * @param x
	 *            The value to add to horizontal (x) coordinates.
	 * @param y
	 *            The value to add to vertical (y) coordinates
	 *            {@link #translate(double, double)}, the value is added to the
	 *            x- and y-coordinate values.
	 */
	public void translate(Double x, Double y) {
		rpc.translate(x, y);
	}

	/**
	 * Scale the current drawing, bigger or smaller.
	 * 
	 * @param x
	 *            Scales the width of the current drawing (1=100%, 0.5=50%,
	 *            2=200%, etc.)
	 * @param y
	 *            Scales the height of the current drawing (1=100%, 0.5=50%,
	 *            2=200%, etc.)
	 */
	public void scale(Double x, Double y) {
		rpc.scale(x, y);
	}

	/**
	 * Saves the state of the current context.
	 */
	public void saveContext() {
		rpc.saveContext();
	}

	/**
	 * Returns previously saved path state and attributes.
	 */
	public void restoreContext() {
		rpc.restoreContext();
	}

	/**
	 * Sets the color, gradient, or pattern used for strokes.
	 * 
	 * @param rgb
	 *            the new stroke style.
	 */
	public void setStrokeStyle(String rgb) {
		rpc.setStrokeStyle(rgb);
	}

	/**
	 * Sets the stroke RGB color used for strokes.
	 * 
	 * @param r
	 *            the red component of the stroke color.
	 * @param g
	 *            the green component of the stroke color.
	 * @param b
	 *            the blue component of the stroke color.
	 */
	public void setStrokeStyle(int r, int g, int b) {
		setStrokeStyle("rgb(" + r + "," + g + "," + b + ")");
	}

	/**
	 * Begins a path, or resets the current path.
	 */
	public void beginPath() {
		rpc.beginPath();
	}

	/**
	 * Creates a path from the current point back to the starting point.
	 */
	public void closePath() {
		rpc.closePath();
	}

	/**
	 * Clear the canvas content.
	 */
	public void clear() {
		rpc.clear();
	}

	/**
	 * Sets the current alpha or transparency value of the drawing.
	 * 
	 * The alpha value must be a number between 0.0 (fully transparent) and 1.0
	 * (no transparancy).
	 * 
	 * @param alpha
	 *            the new global alpha value.
	 */
	public void setGlobalAlpha(Double alpha) {
		rpc.setGlobalAlpha(alpha);
	}

	/**
	 * Sets how a source (new) image are drawn onto a destination (existing)
	 * image.
	 * 
	 * source image = drawings you are about to place onto the canvas.
	 * 
	 * destination image = drawings that are already placed onto the canvas.
	 * 
	 * Possible operation names are:
	 * 
	 * "source-over" Default. Displays the source image over the destination
	 * image
	 * 
	 * "source-atop" Displays the source image on top of the destination image.
	 * The part of the source image that is outside the destination image is not
	 * shown
	 * 
	 * "source-in" Displays the source image in to the destination image. Only
	 * the part of the source image that is INSIDE the destination image is
	 * shown, and the destination image is transparent
	 * 
	 * "source-out" Displays the source image out of the destination image. Only
	 * the part of the source image that is OUTSIDE the destination image is
	 * shown, and the destination image is transparent
	 * 
	 * "destination-over" Displays the destination image over the source image
	 * 
	 * destination-atop Displays the destination image on top of the source
	 * image. The part of the destination image that is outside the source image
	 * is not shown
	 * 
	 * "destination-in" Displays the destination image in to the source image.
	 * Only the part of the destination image that is INSIDE the source image is
	 * shown, and the source image is transparent
	 * 
	 * "destination-out" Displays the destination image out of the source image.
	 * Only the part of the destination image that is OUTSIDE the source image
	 * is shown, and the source image is transparent
	 * 
	 * "lighter" Displays the source image + the destination image
	 * 
	 * "copy" Displays the source image. The destination image is ignored
	 * 
	 * "xor" The source image is combined by using an exclusive OR with the
	 * destination image
	 * 
	 * @param mode
	 *            the new global composite operation name.
	 */
	public void setGlobalCompositeOperation(String mode) {
		rpc.setGlobalCompositeOperation(mode);
	}

	/**
	 * Sets the gradient name to be used to fill the drawing.
	 * 
	 * @param gradient
	 *            the new gradient name.
	 */
	public void setGradientFillStyle(String gradient) {
		rpc.setGradientFillStyle(gradient);
	}

	/**
	 * Creates a linear gradient object with the specified name The gradient can
	 * be used to fill rectangles, circles, lines, text, etc.
	 * 
	 * Use the gradient name as a parameter for {@link #setStrokeStyle(String)}
	 * or {@link #setFillStyle(String)}.
	 * 
	 * Use the {@link #addColorStop(String, double, String)} method to specify
	 * different colors, and where to position the colors in the gradient
	 * object.
	 * 
	 * @param name
	 *            the gradient name.
	 * @param x0
	 *            The x-coordinate of the start point of the gradient.
	 * @param y0
	 *            The y-coordinate of the start point of the gradient.
	 * @param x1
	 *            The x-coordinate of the end point of the gradient.
	 * @param y1
	 *            The y-coordinate of the end point of the gradient.
	 */
	public void createLinearGradient(String name, Double x0, Double y0,
			Double x1, Double y1) {
		rpc.createLinearGradient(name, x0, y0, x1, y1);
	}

	/**
	 * Creates a radial gradient object with the specified name The gradient can
	 * be used to fill rectangles, circles, lines, text, etc.
	 * 
	 * Use the gradient name as a parameter for {@link #setStrokeStyle(String)}
	 * or {@link #setFillStyle(String)}.
	 * 
	 * Use the {@link #addColorStop(String, double, String)} method to specify
	 * different colors, and where to position the colors in the gradient
	 * object.
	 * 
	 * @param name
	 *            the name of the gradient.
	 * @param x0
	 *            The x-coordinate of the starting circle of the gradient.
	 * @param y0
	 *            The y-coordinate of the starting circle of the gradient.
	 * @param r0
	 *            The radius of the starting circle.
	 * @param x1
	 *            The x-coordinate of the ending circle of the gradient.
	 * @param y1
	 *            The y-coordinate of the ending circle of the gradient.
	 * @param r1
	 *            The radius of the ending circle.
	 */
	public void createRadialGradient(String name, Double x0, Double y0,
			Double r0, Double x1, Double y1, Double r1) {
		rpc.createRadialGradient(name, x0, y0, r0, x1, y1, r1);
	}

	/**
	 * Sets the gradient used for strokes.
	 * 
	 * @param gradient
	 *            the gradient name.
	 */
	public void setGradientStrokeStyle(String gradient) {
		rpc.setGradientStrokeStyle(gradient);
	}

	/**
	 * Sets the gradient name to be used to fill the drawing.
	 * 
	 * @param gradient
	 *            the new gradient name.
	 */
	public void addColorStop(String gradient, Double offset, String color) {
		rpc.addColorStop(gradient, offset, color);
	}

	/**
	 * Draws a cubic Bezier curve from the current point to the point (x, y),
	 * with control points (cp1x, cp1y) and (cp2x, cp2y).
	 * 
	 * @param cp1x
	 *            - the x coordinate of the first control point
	 * @param cp1y
	 *            - the y coordinate of the first control point
	 * @param cp2x
	 *            - the x coordinate of the second control point
	 * @param cp2y
	 *            - the y coordinate of the second control point
	 * @param x
	 *            - the x coordinate of the end point.
	 * @param y
	 *            - the y coordinate of the end point.
	 */
	public void bezierCurveTo(double cp1x, double cp1y, double cp2x,
			double cp2y, double x, double y) {
		rpc.bezierCurveTo(cp1x, cp1y, cp2x, cp2y, x, y);
	}

	// @Override
	// public CanvasPlusState getState() {
	// return (CanvasPlusState) super.getState();
	// }
}