package com.alsnightsoft.vaadin.widgets.canvasplus.client.canvasplus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alsnightsoft.vaadin.widgets.canvasplus.CanvasPlus;
import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.CanvasGradient;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.dom.client.ImageElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ErrorEvent;
import com.google.gwt.event.dom.client.ErrorHandler;
import com.google.gwt.event.dom.client.LoadEvent;
import com.google.gwt.event.dom.client.LoadHandler;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.client.MouseEventDetailsBuilder;
import com.vaadin.client.communication.RpcProxy;
import com.vaadin.client.communication.StateChangeEvent;
import com.vaadin.client.ui.AbstractComponentConnector;
import com.vaadin.client.ui.PostLayoutListener;
import com.vaadin.client.ui.SimpleManagedLayout;
import com.vaadin.shared.MouseEventDetails;
import com.vaadin.shared.ui.Connect;

@Connect(CanvasPlus.class)
public class CanvasPlusConnector extends AbstractComponentConnector implements SimpleManagedLayout, PostLayoutListener {

	private static final long serialVersionUID = 1757943567198408520L;

	private boolean redraw = false;
	private final List<Command> commands;

	private final Map<String, CanvasGradient> gradients = new HashMap<String, CanvasGradient>();

	private final CanvasPlusServerRpc rpc = RpcProxy.create(CanvasPlusServerRpc.class, this);

	public CanvasPlusConnector() {
		commands = new ArrayList<Command>();
	}

	public void addCommand(Command c) {
		if (commands.add(c)) {
			c.execute();
		}
	}

	public void clearCanvasCommands() {
		commands.clear();
	}

	@Override
	protected void init() {
		super.init();

		getWidget().addMouseMoveHandler(new MouseMoveHandler() {
			@Override
			public void onMouseMove(MouseMoveEvent event) {
				MouseEventDetails med = MouseEventDetailsBuilder.buildMouseEventDetails(event.getNativeEvent(), getWidget().getElement());
				rpc.onMouseMove(med);
			}
		});

		getWidget().addMouseDownHandler(new MouseDownHandler() {
			@Override
			public void onMouseDown(MouseDownEvent event) {
				MouseEventDetails med = MouseEventDetailsBuilder.buildMouseEventDetails(event.getNativeEvent(), getWidget().getElement());
				rpc.onClickDown(med);
			}
		});

		getWidget().addMouseUpHandler(new MouseUpHandler() {
			@Override
			public void onMouseUp(MouseUpEvent event) {
				MouseEventDetails med = MouseEventDetailsBuilder.buildMouseEventDetails(event.getNativeEvent(), getWidget().getElement());
				rpc.onClickUp(med);
			}
		});

		getWidget().addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				MouseEventDetails med = MouseEventDetailsBuilder.buildMouseEventDetails(event.getNativeEvent(), getWidget().getElement());
				rpc.onClick(med);
			}
		});

		registerRpc(CanvasPlusClientRpc.class, new CanvasPlusClientRpc() {

			private static final long serialVersionUID = 722122751815201973L;
			private final Context2d ctx = getWidget().getContext2d();

			@Override
			public void loadImages(final String[] urls) {
				final List<String> imgToLoad = new ArrayList<String>();
				imgToLoad.addAll(Arrays.asList(urls));

				for (final String url : imgToLoad) {
					final Image image = new Image(url);

					image.addLoadHandler(new LoadHandler() {
						@Override
						public void onLoad(LoadEvent event) {
							imgToLoad.remove(url);
							if (imgToLoad.isEmpty()) {
								rpc.imgLoaded();
							}
						}
					});

					image.addErrorHandler(new ErrorHandler() {
						@Override
						public void onError(ErrorEvent event) {
							imgToLoad.remove(url);
							if (imgToLoad.isEmpty()) {
								rpc.imgLoaded();
							}
						}
					});

					RootPanel.get().add(image);
					image.setVisible(false);
				}
			}

			@Override
			public void drawImage(final String url, final Double offsetX, final Double offsetY) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.drawImage(ImageElement.as(new Image(url).getElement()), offsetX, offsetY);
					}
				});
			}

			@Override
			public void drawImage2(final String url, final Double offsetX, final Double offsetY, final Double imageWidth, final Double imageHeight) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.drawImage(ImageElement.as(new Image(url).getElement()), offsetX, offsetY, imageWidth, imageHeight);
					}
				});
			}

			@Override
			public void drawImage3(final String url, final Double sourceX, final Double sourceY, final Double sourceWidth, final Double sourceHeight, final Double destX, final Double destY,
					final Double destWidth, final Double destHeight) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.drawImage(ImageElement.as(new Image(url).getElement()), sourceX, sourceY, sourceWidth, sourceHeight, destX, destY, destWidth, destHeight);
					}
				});
			}

			@Override
			public void fill() {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.fill();
					}
				});
			}

			@Override
			public void fillRect(final Double startX, final Double startY, final Double rectWidth, final Double rectHeight) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.fillRect(startX, startY, rectWidth, rectHeight);
					}
				});
			}

			@Override
			public void fillText(final String text, final Double x, final Double y, final Double maxWidth) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.fillText(text, x, y, maxWidth);
					}
				});
			}

			@Override
			public void setFont(final String font) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.setFont(font);
					}
				});
			}

			@Override
			public void setTextBaseline(final String textBaseline) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.setTextBaseline(textBaseline);
					}
				});
			}

			@Override
			public void lineTo(final Double x, final Double y) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.lineTo(x, y);
					}
				});
			}

			@Override
			public void moveTo(final Double x, final Double y) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.moveTo(x, y);
					}
				});
			}

			@Override
			public void quadraticCurveTo(final Double cpx, final Double cpy, final Double x, final Double y) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.quadraticCurveTo(cpx, cpy, x, y);
					}
				});
			}

			@Override
			public void rect(final Double startX, final Double startY, final Double rectWidth, final Double rectHeight) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.rect(startX, startY, rectWidth, rectHeight);
					}
				});
			}

			@Override
			public void rotate(final Double angle) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.rotate(angle);
					}
				});
			}

			@Override
			public void setFillStyle(final String color) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.setFillStyle(color);
					}
				});
			}

			@Override
			public void setLineCap(final String lineCap) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.setLineCap(lineCap);
					}
				});
			}

			@Override
			public void setLineJoin(final String lineJoin) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.setLineJoin(lineJoin);
					}
				});
			}

			@Override
			public void setLineWidth(final Double width) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.setLineWidth(width);
					}
				});
			}

			@Override
			public void setMiterLimit(final Double miterLimit) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.setMiterLimit(miterLimit);
					}
				});
			}

			@Override
			public void stroke() {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.stroke();
					}
				});
			}

			@Override
			public void strokeRect(final Double startX, final Double startY, final Double strokeWidth, final Double strokeHeight) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.strokeRect(startX, startY, strokeWidth, strokeHeight);
					}
				});
			}

			@Override
			public void transform(final Double m11, final Double m12, final Double m21, final Double m22, final Double dx, final Double dy) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.transform(m11, m12, m21, m22, dx, dy);
					}
				});
			}

			@Override
			public void arc(final Double x, final Double y, final Double radius, final Double startAngle, final Double endAngle, final Boolean antiClockwise) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.arc(x, y, radius, startAngle, endAngle, antiClockwise);
					}
				});
			}

			@Override
			public void translate(final Double x, final Double y) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.translate(x, y);
					}
				});
			}

			@Override
			public void scale(final Double x, final Double y) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.scale(x, y);
					}
				});
			}

			@Override
			public void saveContext() {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.save();
					}
				});
			}

			@Override
			public void restoreContext() {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.restore();
					}
				});
			}

			@Override
			public void setStrokeStyle(final String rgb) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.setStrokeStyle(rgb);
					}
				});
			}

			@Override
			public void beginPath() {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.beginPath();
					}
				});
			}

			@Override
			public void closePath() {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.closePath();
					}
				});
			}

			@Override
			public void clear() {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.clearRect(0, 0, getWidget().getCoordinateSpaceWidth(), getWidget().getCoordinateSpaceHeight());
						clearCanvasCommands();
					}
				});
			}

			@Override
			public void setGlobalAlpha(final Double alpha) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.setGlobalAlpha(alpha);
					}
				});
			}

			@Override
			public void setGlobalCompositeOperation(final String mode) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.setGlobalCompositeOperation(mode);
					}
				});
			}

			@Override
			public void setGradientFillStyle(final String gradient) {
				addCommand(new Command() {
					@Override
					public void execute() {
						if (gradients.containsKey(gradient)) {
							ctx.setFillStyle(gradients.get(gradient));
						} else {
							System.out.println("GradientFillStyle - Gradient not foud with name " + gradient);
						}
					}
				});
			}

			@Override
			public void createLinearGradient(final String name, final Double x0, final Double y0, final Double x1, final Double y1) {
				addCommand(new Command() {
					@Override
					public void execute() {
						CanvasGradient newGradient = ctx.createLinearGradient(x0, y0, x1, y1);
						gradients.put(name, newGradient);
					}
				});
			}

			@Override
			public void createRadialGradient(final String name, final Double x0, final Double y0, final Double r0, final Double x1, final Double y1, final Double r1) {
				addCommand(new Command() {
					@Override
					public void execute() {
						CanvasGradient newGradient = ctx.createRadialGradient(x0, y0, r0, x1, y1, r1);
						gradients.put(name, newGradient);
					}
				});
			}

			@Override
			public void setGradientStrokeStyle(final String gradient) {
				addCommand(new Command() {
					@Override
					public void execute() {
						if (gradients.containsKey(gradient)) {
							ctx.setStrokeStyle(gradients.get(gradient));
						} else {
							System.out.println("GradientStrokeStyle - Gradient not found with name " + gradient);
						}
					}
				});
			}

			@Override
			public void addColorStop(final String gradient, final Double offset, final String color) {
				addCommand(new Command() {
					@Override
					public void execute() {
						if (gradients.containsKey(gradient)) {
							gradients.get(gradient).addColorStop(offset, color);
						} else {
							System.out.println("ColorStop - Gradient not found with name " + gradient);
						}
					}
				});
			}

			@Override
			public void bezierCurveTo(final double cp1x, final double cp1y, final double cp2x, final double cp2y, final double x, final double y) {
				addCommand(new Command() {
					@Override
					public void execute() {
						ctx.bezierCurveTo(cp1x, cp1y, cp2x, cp2y, x, y);
					}
				});
			}
		});

	}

	@Override
	protected Widget createWidget() {
		return Canvas.createIfSupported();
	}

	@Override
	public Canvas getWidget() {
		return (Canvas) super.getWidget();
	}

	@Override
	public CanvasPlusState getState() {
		return (CanvasPlusState) super.getState();
	}

	@Override
	public void onStateChanged(StateChangeEvent stateChangeEvent) {
		super.onStateChanged(stateChangeEvent);
	}

	@Override
	public void layout() {
		if (getWidget().getElement().getOffsetHeight() != getWidget().getCoordinateSpaceHeight()) {
			getWidget().setCoordinateSpaceHeight(getWidget().getElement().getOffsetHeight());
			redraw = true;
		}
		if (getWidget().getElement().getOffsetWidth() != getWidget().getCoordinateSpaceWidth()) {
			getWidget().setCoordinateSpaceWidth(getWidget().getElement().getOffsetWidth());
			redraw = true;
		}
	}

	@Override
	public void postLayout() {
		if (redraw) {
			for (Command c : commands) {
				c.execute();
			}
			redraw = false;
		}
	}

}