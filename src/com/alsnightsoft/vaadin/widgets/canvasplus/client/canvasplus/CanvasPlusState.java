package com.alsnightsoft.vaadin.widgets.canvasplus.client.canvasplus;

import com.vaadin.shared.AbstractComponentState;

public class CanvasPlusState extends AbstractComponentState {

	private static final long serialVersionUID = -791871189570425225L;

	public String text = "This is CanvasPlus";

}