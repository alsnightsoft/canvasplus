package com.alsnightsoft.vaadin.widgets.canvasplus.client.canvasplus;

import com.vaadin.shared.MouseEventDetails;
import com.vaadin.shared.communication.ServerRpc;

public interface CanvasPlusServerRpc extends ServerRpc {

	public void onClick(MouseEventDetails mouseDetails);

	public void onClickDown(MouseEventDetails mouseDetails);

	public void onClickUp(MouseEventDetails mouseDetails);

	public void onMouseMove(MouseEventDetails mouseDetails);
	
	public void imgLoaded();

}