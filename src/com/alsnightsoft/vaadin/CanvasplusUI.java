package com.alsnightsoft.vaadin;

import javax.servlet.annotation.WebServlet;

import com.alsnightsoft.vaadin.widgets.canvasplus.CanvasPlus;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.MouseEventDetails;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
@Theme("canvasplus")
public class CanvasplusUI extends UI {

	// https://code.google.com/p/gwt-examples/wiki/gwt_hmtl5
	
	final VerticalLayout mainLayout = new VerticalLayout();

	CanvasPlus canvas = new CanvasPlus();
	final Button btnImage = new Button("Click me to draw Image");
	final Button btnLine = new Button("Click me to draw Lines");

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = CanvasplusUI.class, widgetset = "com.alsnightsoft.vaadin.widgets.canvasplus.CanvasplusWidgetset")
	public static class Servlet extends VaadinServlet {
	}

	@Override
	protected void init(VaadinRequest request) {
		mainLayout.setSpacing(true);
		mainLayout.setMargin(true);

		btnLine.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				drawLines();
			}
		});
		
		canvas.setWidth("600");
		canvas.setHeight("600");

		canvas.loadImages(new String[] { "http://webapp.org.ua/wp-content/uploads/2011/10/gwtlogo.jpg" });

		canvas.addClickListener(new CanvasPlus.CanvasClickListener() {
			@Override
			public void onClick(MouseEventDetails mouseDetails) {
				System.out.println("Click");
				System.out.println("Data " + mouseDetails.getRelativeX() + "" + mouseDetails.getRelativeY());
			}
		});

		canvas.addMouseMoveListener(new CanvasPlus.CanvasMouseMoveListener() {
			@Override
			public void onMove(MouseEventDetails mouseDetails) {
				System.out.println("Data " + mouseDetails.getRelativeX() + "" + mouseDetails.getRelativeY());
			}
		});

		canvas.addClickUpListener(new CanvasPlus.CanvasClickUpListener() {
			@Override
			public void onClickUp(MouseEventDetails mouseDetails) {
				System.out.println("Click Up");
			}
		});

		canvas.addClickDownListener(new CanvasPlus.CanvasClickDownListener() {
			@Override
			public void onClickDown(MouseEventDetails mouseDetails) {
				System.out.println("Click Down");
			}
		});

		canvas.addImageLoadListener(new CanvasPlus.CanvasImageLoadListener() {
			@Override
			public void onImageLoad() {
				drawImage();
				btnImage.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						drawImage();
					}
				});
			}
		});

		mainLayout.addComponent(new Label("Hello Vaadin."));
		mainLayout.addComponent(btnImage);
		mainLayout.addComponent(btnLine);
		mainLayout.addComponent(canvas);
		mainLayout.setComponentAlignment(canvas, Alignment.MIDDLE_CENTER);

		setContent(mainLayout);
	}
	
	private void drawLines() {
		canvas.saveContext();
		canvas.clear();
		canvas.moveTo(0, 0);
		
		for (int i = 0; i < 600; i += 10) {
			canvas.moveTo(i, 0);
			canvas.lineTo(i, 600);
		}

		for (int i = 0; i < 600; i += 10) {
			canvas.moveTo(0, i);
			canvas.lineTo(600, i);
		}

		canvas.setStrokeStyle("#f00");
		canvas.stroke();
		canvas.restoreContext();
	}

	private void drawImage() {
		canvas.saveContext();
		canvas.clear();
		canvas.moveTo(0, 0);
		double x = 0, y = 0;
		canvas.drawImage("http://webapp.org.ua/wp-content/uploads/2011/10/gwtlogo.jpg", x, y);
		canvas.restoreContext();
	}

}